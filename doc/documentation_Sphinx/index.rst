.. dbe documentation master file, created by
   sphinx-quickstart on Mon Jan 10 15:27:36 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###############################
Welcome to "dbe" documentation!
###############################


.. include:: small_beginning_note.rst 

----


Contents:
=========

**User's Guide**

.. toctree::
   :maxdepth: 2

   user_guide/dbe_user_guide

----

**Video Tutorials**

.. toctree::

   user_guide/video_tutorials.rst

----

**User's Support**

   * `DBE Issue Tracking (JIRA) <https://its.cern.ch/jira/browse/ATLASDBE>`_
   * Do you have a *feature request* or want to report a *bug*? **Fill a ticket** clicking on the blue onglet on the right of this web page!



----

**DBE ATLAS Twiki**

`DBE in the ATLAS Twiki <https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltConfigurationDBeditor>`_

----

**Releases' history and valdation**

.. toctree::
   :maxdepth: 1

   developers_doc/changes
   developers_doc/validation_tests

----

**About DBE**

.. toctree::
   :maxdepth: 2

   about_dbe

----

**Code Documentation (Doxygen)**

http://atlasdaq.cern.ch/dbe/doxygen_doc/html/

----

**Developer's Notes**

.. toctree::
   :maxdepth: 1

   developers_doc/dev_guide_index

   

----




Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`

