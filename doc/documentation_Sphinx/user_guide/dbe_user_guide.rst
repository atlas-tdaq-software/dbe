###################
"dbe" User's Guide 
###################


.. include:: ../small_beginning_note.rst 

----

.. contents::



.. a chapter on how to start with the dbe editor

.. include:: how_to_start.rst

.. a chapter on settings:

.. include:: dbe_settings.rst

.. Point1 instructions

.. include:: dbe_at_P1.rst


.. a chapter on loading a DB

.. include:: loading_db.rst

.. explaining file permission flags in the File Widget table

.. include:: file_permissions.rst

.. a chapter on navigation through DB objects

.. include:: db_navigation.rst



.. a chapter on creating new DB objects

.. include:: create_object.rst

.. a chapter on saving DB changes

.. include:: save_db_changes.rst


.. a chapter on building custom user plugins

.. .. include:: plugins.rst


.. how to compile and run the HEAD version

.. include:: how_to_HEAD_version.rst


.. how to use users' scripts in the Python interface

.. include:: python_plugin_interface.rst



